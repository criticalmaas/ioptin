// import React, { useState } from "react"
import React from "react"
import { useAuth0 } from "../react-auth0-spa"
import * as SurveyJS from "survey-react"
import "survey-react/survey.css"
import SurveyModel from "./survey-model/quick-check.json"
// import gql from "graphql-tag"

var zipcodes = require("zipcodes")
// import { useQuery } from "@apollo/react-hooks"

// const GET_LIST = gql`
//   query GetList {
//     human_data {
//       auth0_user_id
//       created_at
//       id
//       zip_postal_code
//     }
//   }
// `

// let data = {}

// ensures that the entry is cleaned and uppercased and is Canadian for a string
const cleanZip = zip => {
  if (
    zip !== null &&
    zip !== undefined &&
    typeof zip === "string" &&
    isNaN(zip.charAt(0))
  ) {
    return zip.slice(0, 3).toUpperCase()
  }
  return zip
}

const Survey = () => {
  const isZipCode = zip => {
    try {
      var zipLookUpData = zipcodes.lookup(cleanZip(zip[0]))
      //setZipData(zipLookUpData)
      // data = { ...data, ...zipLookUpData }
      return zipLookUpData ? true : false
    } catch (e) {
      return false
    }
  }

  const onComplete = result => {
    // console.log("data OC", data)
    console.log(result.data)
    // setSurveyData(result.data)
    // data = { ...data, ...result.data }
  }

  const onValueChanged = result => {
    // console.log("data VC", data)
    console.log(result.data)
    // setSurveyData(result.data)
    // data = { ...data, ...result.data }
    // console.log("data VC", data)
  }

  const { loading, user } = useAuth0()
  // const [surveyData, setSurveyData] = useState({})
  // const [zipData, setZipData] = useState({})

  // const { queryLoading, error, data } = useQuery(GET_LIST)

  if (loading || !user) {
    // if (loading || queryLoading || !user) {
    return <div>Loading...</div>
  }

  // if (error) {
  //   return <div>Error!</div>
  // }

  var model = new SurveyJS.Model(SurveyModel)

  SurveyJS.FunctionFactory.Instance.register("isZipCode", isZipCode)
  return (
    <>
      <h1 className="is-size-3">Survey</h1>
      <div className="surveyjs">
        <SurveyJS.Survey
          model={model}
          onComplete={onComplete}
          onValueChanged={onValueChanged}
        />
      </div>
      {/* <pre>
        {JSON.stringify(
          {
            authData: { ...user },
            ...data,
          },
          null,
          2
        )}
      </pre> */}
    </>
  )
}

export default Survey
