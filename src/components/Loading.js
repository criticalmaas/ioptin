import React from "react"

const Loading = () => {
  return <progress className="progress is-small is-info" max="100"></progress>
}

export default Loading
