import React from "react"
import "../../App.scss"
import NavBar from "../NavBar"

function Layout({ children }) {
  return (
    <>
      <NavBar />
      <section className="section">
        <div className="container">{children}</div>
      </section>
    </>
  )
}

export default Layout
