import React, { useState } from "react"
import { useAuth0 } from "../react-auth0-spa"
import { NavLink } from "react-router-dom"
import logo from "../logo_181x36.png"
const NavBar = () => {
  const { isAuthenticated, loginWithRedirect, logout } = useAuth0()
  const [menu, setMenu] = useState(false)

  return (
    <>
      <nav
        className="navbar is-white is-spaced"
        role="navigation"
        aria-label="main navigation"
        style={{ borderBottom: "1px solid #f2f2f2" }}
      >
        <div className="container">
          <div className="navbar-brand">
            <NavLink to="/" className="navbar-item">
              <img src={logo} alt="I Opt In" />
            </NavLink>
            <a
              role="button"
              aria-label="menu"
              aria-expanded="false"
              className={`navbar-burger burger ${menu ? "is-active" : ""}`}
              data-target="navMenu"
              onClick={() => setMenu(!menu)}
            >
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>

          <div
            id="navMenu"
            className={`navbar-menu ${menu ? "is-active" : ""}`}
            onClick={() => setMenu(false)}
          >
            <div className="navbar-start" />

            <div className="navbar-end">
              <NavLink
                to="/survey"
                activeClassName="is-active"
                exact={true}
                className="navbar-item"
              >
                Share Your Info
              </NavLink>

              <NavLink
                to="/stats"
                activeClassName="is-active"
                exact={true}
                className="navbar-item"
              >
                See Stats
              </NavLink>

              <div className="navbar-item has-dropdown is-hoverable">
                <a className="navbar-link">Other</a>

                <div className="navbar-dropdown">
                  <NavLink
                    to="/about"
                    activeClassName="is-active"
                    exact={true}
                    className="navbar-item"
                  >
                    About this project
                  </NavLink>
                  <NavLink
                    to="/profile"
                    activeClassName="is-active"
                    exact={true}
                    className="navbar-item"
                  >
                    See your profile
                  </NavLink>
                  <NavLink
                    to="/contact"
                    activeClassName="is-active"
                    exact={true}
                    className="navbar-item"
                  >
                    Contact
                  </NavLink>

                  <hr className="navbar-divider" />
                  <NavLink
                    to="/report-issue"
                    activeClassName="is-active"
                    exact={true}
                    className="navbar-item"
                  >
                    Report an issue
                  </NavLink>
                </div>
              </div>
              <div className="navbar-item">
                <div className="buttons">
                  {!isAuthenticated && (
                    <NavLink
                      to="/"
                      className="button is-warning"
                      onClick={() => loginWithRedirect({})}
                    >
                      Log in
                    </NavLink>
                  )}
                  {isAuthenticated && (
                    <NavLink
                      to="/"
                      className="button is-warning"
                      onClick={() => logout()}
                    >
                      Log out
                    </NavLink>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </>
  )
}

export default NavBar
