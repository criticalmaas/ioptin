import React from "react"
import { useAuth0 } from "../react-auth0-spa"

import gql from "graphql-tag"
import { useQuery } from "@apollo/react-hooks"

const GET_LIST = gql`
  query GetList {
    human_data {
      auth0_user_id
      created_at
      id
      zip_postal_code
    }
  }
`

const Profile = () => {
  const { loading, user } = useAuth0()
  const { queryLoading, error, data } = useQuery(GET_LIST)

  if (loading || queryLoading || !user) {
    return <div>Loading...</div>
  }

  if (error) {
    return <div>Error!</div>
  }

  return (
    <>
      <h1 className="is-size-3">Profile</h1>
      <ul>
        {data &&
          data.human_data.map((datum, idx) => {
            return (
              <li key={idx}>
                {datum.auth0_user_id} : {datum.zip_postal_code}
              </li>
            )
          })}
      </ul>
      <h2>{user.name}</h2>
      <p>{user.email}</p>
      <code>{JSON.stringify(user, null, 2)}</code>
    </>
  )
}

export default Profile
