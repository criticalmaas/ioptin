import React, { Suspense } from "react"
import { Router, Route, Switch } from "react-router-dom"
import history from "./utils/history"

import { useAuth0 } from "./react-auth0-spa"

import Layout from "./components/layout"
import PrivateRoute from "./components/PrivateRoute"
import Home from "./components/Home"
import Loading from "./components/Loading"
import Survey from "./components/Survey"
import Profile from "./components/Profile"

function App() {
  const { loading } = useAuth0()

  if (loading) {
    return <Loading />
  }

  return (
    <Router history={history}>
      <Layout>
        <Switch>
          <Route path="/" exact component={Home} />
          <PrivateRoute path="/survey" component={Survey} />
          <PrivateRoute path="/profile" component={Profile} />
        </Switch>
      </Layout>
    </Router>
  )
}

export default App
