import React from "react"

import ApolloClient from "apollo-client"
import { InMemoryCache } from "apollo-cache-inmemory"
import { HttpLink } from "apollo-link-http"
import { setContext } from "@apollo/link-context"
import { ApolloProvider } from "@apollo/react-hooks"
import { useAuth0 } from "./react-auth0-spa"

function Apollo({ children }) {
  const { getTokenSilently, getIdTokenClaims } = useAuth0()

  const httpLink = new HttpLink({
    uri: "https://ioptin.herokuapp.com/v1/graphql",
  })

  const authLink = setContext(async () => {
    await getTokenSilently()
    const token = await getIdTokenClaims()
    console.log(token.__raw)
    return {
      headers: {
        Authorization: `Bearer ${token.__raw}`,
      },
    }
  })

  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
    connectToDevTools: true,
  })

  return <ApolloProvider client={client}>{children}</ApolloProvider>
}

export default Apollo
